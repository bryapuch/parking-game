/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parking.game;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;

/**
 *
 * @author Bryan
 */
public class Ventanas {
    
    public static double GAME_WIDTH = 600;
    public static double GAME_HEIGHT = 600;
    public static double desplazamiento = 5;
    public static Jugador jugador = new Jugador();
    public HashSet<Auto> autos = new HashSet<>();
    private ObservableList<Jugador> ListaJugadores = FXCollections.observableArrayList();
    TableView<Jugador> table;
    public static String rutaI;
    LecturayEscritura lyc = new LecturayEscritura();
    vNivel1 n1= new vNivel1();
    public static Scene scene;
    Auto auto;
    
    Ventanas() {
        
    }
    
    public StackPane MenuPrincipal(Stage primaryStage) {
        StackPane v = new StackPane();//ventana Principal
        VBox vb = new VBox(20);
        ListaJugadores = FXCollections.observableArrayList();
        ImageView perfil = new ImageView(new Image(Ventanas.class.getResource("fondoAuto.png").toExternalForm()));
        perfil.setFitHeight(400);
        perfil.setFitWidth(600);

        //Creacion de Botones
        Button btNuevo = new Button("Nuevo Juego");
        Button btLista = new Button("Lista de Jugadores");
        Button btSalir = new Button("Salir");

        //Dimension de los botones
        btNuevo.setMinSize(160, 40);
        btLista.setMinSize(160, 40);
        btSalir.setMinSize(160, 40);

        //Dimensiones de las letras de los botones
        btNuevo.setFont(Font.font(15));
        btLista.setFont(Font.font(15));
        btSalir.setFont(Font.font(15));

        //Colores a los botones
        btNuevo.setStyle("-fx-background-color: CHARTREUSE");
        btLista.setStyle("-fx-background-color: CHARTREUSE");
        btSalir.setStyle("-fx-background-color: CHARTREUSE");
        

        //Accion del Button Salir
        btSalir.setOnAction((ActionEvent event) -> {
            Scene Scene = new Scene(ventanaSalida(primaryStage), 300, 200);
            primaryStage.setTitle("Confirmacion");
            primaryStage.setScene(Scene);
            primaryStage.show();
            
        });
        //Accion del Boton Nuevo Juego
        btNuevo.setOnAction((ActionEvent event) -> {
            Scene scene = new Scene(ventanaJugador(primaryStage), 450, 350);
            primaryStage.setTitle("Jugador");
            primaryStage.setScene(scene);
            primaryStage.show();
            
        });

        //Accion del boton Lista de Jugadores
        btLista.setOnAction((ActionEvent event) -> {
            try {btSalir.setOnAction((ActionEvent event1) -> {
            Scene Scene = new Scene(ventanaSalida(primaryStage), 300, 200);
            primaryStage.setTitle("Confirmacion");
            primaryStage.setScene(Scene);
            primaryStage.show();
            
        });
                scene = new Scene(ventanaLista(primaryStage));
                primaryStage.setTitle("Lista de Jugadores");
                primaryStage.setScene(scene);
                primaryStage.show();
            } catch (IOException ex) {
                Logger.getLogger(Ventanas.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        });
        
        vb.getChildren().addAll(btNuevo,btLista,btSalir);
        vb.setAlignment(Pos.CENTER);
        v.getChildren().add(perfil);
        v.getChildren().add(vb);
        v.setAlignment(Pos.CENTER);
        return v;
    }
    
    public GridPane ventanaJugador(Stage stage) {
        GridPane root = new GridPane();
        HBox hb = new HBox();
        Label lbNombre = new Label("Nombre: ");
        TextField tfNombre = new TextField();
        Button btAceptar = new Button("Aceptar");
        
        btAceptar.setMinSize(150, 30);//Dimension del boton Aceptar
        btAceptar.setFont(Font.font(20));//Aumento de fuente del Boton Aceptar
        btAceptar.setAlignment(Pos.CENTER);
        btAceptar.setStyle("-fx-background-color: CHARTREUSE");
        lbNombre.setFont(Font.font(20));//Aumento de fuente de la label Nombre
        tfNombre.setFont(Font.font(20));
        //Agregar los labels y botones al root
        root.add(lbNombre, 0, 0);
        root.add(tfNombre, 1, 0);
        root.add(new Label(""), 1, 2);
        root.add(btAceptar, 1, 3);
        root.setAlignment(Pos.CENTER);
        root.setStyle("-fx-background-color: CORNFLOWERBLUE");

        //Accion del boton Aceptar
        btAceptar.setOnMouseClicked((event) -> {
            
            String nombre = tfNombre.getText();
            
            if (nombre == null) {
                Alert a1 = new Alert(Alert.AlertType.INFORMATION);
                //Agregar lista de jugadores
                a1.setTitle("Escrbir Datos");
                a1.setContentText("Escrbir nombre");
                a1.showAndWait();
            } else {
                jugador = new Jugador(nombre);
                jugador.setNombre(nombre);

                Alert a1 = new Alert(Alert.AlertType.INFORMATION);
                //Agregar lista de jugadores
                a1.setTitle("CONTROL DE JUGADORES");
                a1.setContentText("DATOS GUARDADOS CORRECTAMENTE");
                a1.showAndWait();
            }
            
            scene = new Scene(ventanaSeleccion(stage), 420, 400);
            stage.setTitle("Parking Game");
            stage.setScene(scene);
            stage.show();
        });
        
        return root;
        
    }
    
    public VBox ventanaSalida(Stage stage) {
        VBox root = new VBox();
        root.setSpacing(15.0);
        Label lb = new Label("Desea salir del juego?");
        //Cambio de tamaño de fuente del label lb
        lb.setFont(Font.font(20));
        
        lb.setAlignment(Pos.CENTER);
        HBox hb = new HBox();
        Button btnsi = new Button("SI");
        Button btnno = new Button("NO");
        //Colores a los botones
        btnsi.setStyle("-fx-background-color: CHARTREUSE");
        btnno.setStyle("-fx-background-color: CHARTREUSE");
        //Dimensiones de los botones
        btnsi.setMinSize(100, 50);
        btnno.setMinSize(100, 50);
        //Aumento de tamaño de fuente de los botones
        btnsi.setFont(Font.font(15));
        btnno.setFont(Font.font(15));
        
        hb.getChildren().addAll(btnsi, btnno);
        btnsi.setOnAction(e -> {
            Alert a1 = new Alert(Alert.AlertType.INFORMATION);
            a1.setTitle("CONTROL DE USUARIOS");
            a1.setContentText("Gracias, vuelva pronto");
            a1.showAndWait();
            stage.close();
            
        });
        btnno.setOnAction(e -> {
            Alert a1 = new Alert(Alert.AlertType.INFORMATION);
            a1.setTitle("CONTROL DE USUARIOS");
            a1.setContentText("Operacion Cancelada");
            a1.showAndWait();
            Scene scene = new Scene(MenuPrincipal(stage), 400, 400);
            stage.setTitle("Parking Game");
            stage.setScene(scene);
            stage.show();
            
        });
        
        hb.setAlignment(Pos.CENTER);
        hb.setSpacing(15.0);
        root.setAlignment(Pos.CENTER);
        root.getChildren().addAll(lb, hb);
        root.setStyle("-fx-background-color: CORNFLOWERBLUE");//Color al fondo de la ventana

        return root;
        
    }
    
    public VBox ventanaLista(Stage stage) throws IOException {
        VBox vb = new VBox();

        //Columna nombre
        TableColumn<Jugador, String> nombreColumna = new TableColumn("Nombre");
        nombreColumna.setMinWidth(100);
        nombreColumna.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        //Columna tiempo
        TableColumn<Jugador, String> tiempoColumna = new TableColumn("Tiempo");
        tiempoColumna.setMinWidth(100);
        tiempoColumna.setCellValueFactory(new PropertyValueFactory<>("tiempo"));
        //Columna puntaje
        TableColumn<Jugador, String> puntajeColumna = new TableColumn("Puntaje");
        puntajeColumna.setMinWidth(100);
        puntajeColumna.setCellValueFactory(new PropertyValueFactory<>("puntaje"));
        //Columna fecha
        TableColumn<Jugador, String> fechaColumna = new TableColumn("Fecha");
        fechaColumna.setMinWidth(100);
        fechaColumna.setCellValueFactory(new PropertyValueFactory<>("fecha"));
        //
        table = new TableView<>();
        ListaJugadores = lyc.getJugador();
        if (ListaJugadores != null) {
            table.setItems(lyc.getJugador());
        } else {
            table.setItems(null);
        }
        table.getColumns().addAll(nombreColumna, tiempoColumna, puntajeColumna, fechaColumna);
        
        Button btAceptar = new Button("Aceptar");
        //Dimension del boton Aceptar
        btAceptar.setMinSize(110, 40);
        //Aumento de la fuente
        btAceptar.setFont(Font.font(15));
        
        vb.getChildren().addAll(table, btAceptar);
        vb.setAlignment(Pos.CENTER);
        btAceptar.setOnAction((ActionEvent event) -> {
            scene = new Scene(MenuPrincipal(stage), 420, 400);
            stage.setTitle("Parking Game");
            stage.setScene(scene);
            stage.show();
        });
        return vb;
    }
    
    public VBox ventanaSeleccion(Stage stage) {
        autos.add(new Auto("carro amarillo"));
        autos.add(new Auto("carro azul"));
        autos.add(new Auto("carro gris"));
        autos.add(new Auto("carro rojo"));
        VBox root = new VBox();
        root.setSpacing(50);
        root.setAlignment(Pos.CENTER);
        
          root.setStyle("-fx-background-image: url('fondoAuto.png');"+
                 "-fx-background-size:420 ,400 "); 
        ObservableList<Auto> listaA = FXCollections.observableArrayList(autos);
        ComboBox<Auto> opcAuto = new ComboBox<>(listaA);
        opcAuto.getSelectionModel().selectFirst();
        
        ImageView imauto = new ImageView();
        imauto.setFitHeight(150);
        imauto.setFitWidth(250);
        opcAuto.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Auto>() {
            
            @Override
            public void changed(ObservableValue<? extends Auto> arg0, Auto arg1, Auto arg2) {
                
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        System.out.println("Selected auto: " + arg2.toString());
                        Image no = new Image(Ventanas.class.getResource(arg2.toString() + ".png").toExternalForm());
                        imauto.setImage(no);
                        System.out.println(opcAuto.getValue()+".png");
                        Ventanas.this.rutaI = arg2.toString()+ ".png";                        
                    }
                });
                
            }
        });
        Button next = new Button("Siguiente");
        next.setOnAction((ActionEvent event) -> {
            //scene = new Scene(n1.crearPantalla(stage),GAME_WIDTH,GAME_HEIGHT);
            scene = new Scene(new Pane());
            scene.setRoot(n1.crearPantalla(stage));
            stage.setTitle("Nivel 1");
            stage.setScene(scene);
            stage.show();
            
        });
        
        root.getChildren().addAll(opcAuto, imauto, next);
        return root;
    }

    
}
