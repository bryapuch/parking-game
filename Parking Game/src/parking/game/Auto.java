/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parking.game;

import java.util.Comparator;
import java.util.Objects;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author Bryan
 */
public class Auto {

    private String nombre;
    public ImageView iauto;
    private String nAuto;

    public Auto(){
        //agregar la imagen
        //creando la imagen, le pasamos el ancho, el alto, si queremos mantener el 
        //radio entre el ancho y el alto y que s
        Image img = new Image(Auto.class.getResource(Ventanas.rutaI).toExternalForm(),
                90,
                70,
                true,
                true);
        //agrega imagen al imageView
        iauto = new ImageView(img);
    }
        public void fijarPosicionObjeto(double x, double y){
        //fija la poscion de x con respecto a X y Y usando 
        iauto.setLayoutX(x);
        iauto.setLayoutY(y);
    }
    
    public double getPosicionX(){
        return iauto.getLayoutX();
    }
    
    public double getPosicionY(){
        return iauto.getLayoutY();
    }
    
    public Node getObjeto(){
        return iauto;
    }
    
     @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.nombre);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Auto other = (Auto) obj;
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        return true;
    }


    @Override
    public String toString() {
        return nombre;
    }

    public Auto(String nombre) {
        this.nombre = nombre;
    }

    public void setnAuto(String nAuto) {
        this.nAuto = nAuto;
    }


}