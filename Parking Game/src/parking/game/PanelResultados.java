/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parking.game;

import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;

/**
 *
 * @author Odalys
 */
public class PanelResultados {
    private StackPane root;
    private int puntos;
    private double tiempo;

    public PanelResultados(int puntos, double tiempo) {
        this.puntos = puntos;
        this.tiempo = tiempo;
        createContenido();
    }

    public void createContenido() {
        //creamos un gridPane que tenga los resultados
        GridPane gp = new GridPane();
        gp.addRow(0, new Label("Puntos:"), new Label(String.valueOf(puntos)));
        gp.addRow(1, new Label("Tiempo:"), new Label(String.valueOf(tiempo)));

        //el StackPane coloca los hijos en el centro de el
        root = new StackPane(gp);
        root.setPrefSize(Ventanas.GAME_WIDTH, Ventanas.GAME_HEIGHT);
    }

    public Pane getRoot() {
        return root;
    }

    public int getPuntos() {
        return puntos;
    }

    public void setPuntos(int puntos) {
        this.puntos = puntos;
    }

    public double getTiempo() {
        return tiempo;
    }

    public void setTiempo(double tiempo) {
        this.tiempo = tiempo;
    }
    
    
    
}
