/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parking.game;

import java.util.Calendar;
import javafx.collections.ObservableList;

/**
 *
 * @author Odalys
 */
public class Jugador {
    
     private String nombre;
    private String tiempo;
    private String puntaje;
    private String fecha;
    private ObservableList<Jugador> ListaJugadores;

    //constructor vacio
    Jugador() {
    }
    //constructor con parametros nombre, tiempo, puntaje, fecha
    Jugador(String nombre,String tiempo,String puntaje,String fecha){
        this.nombre = nombre;
        this.tiempo = tiempo;
        this.puntaje = puntaje;
        this.fecha = fecha;
    }
    //Constructor con parametos de nombre
    Jugador(String nombre){
        this.nombre = nombre;
        this.fecha = obtenerFecha();
        this.tiempo = "00:00:00";
        this.puntaje = "0"; 
    }
    //Constructor con parametros de nombre, tiempo,puntaje
    Jugador(String nombre,String tiempo, String puntaje){
        this.nombre = nombre;
        this.tiempo = tiempo;
        this.puntaje = puntaje;
        this.fecha = obtenerFecha();
    }
    //metodo de obtener fecha
    public String obtenerFecha() {
        Calendar c = Calendar.getInstance();
        String fech = Integer.toString(c.get(Calendar.DATE)) + "/" + Integer.toString( c.get(Calendar.MONTH) +1 ) + "/" + Integer.toString(c.get(Calendar.YEAR)) ;
        return fech;
    }
    
    //getter y setters

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTiempo() {
        return tiempo;
    }

    public void setTiempo(String tiempo) {
        this.tiempo = tiempo;
    }

    public String getPuntaje() {
        return puntaje;
    }

    public void setPuntaje(String puntaje) {
        this.puntaje = puntaje;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public ObservableList<Jugador> getListaJugadores() {
        return ListaJugadores;
    }

    public void setListaJugadores(ObservableList<Jugador> ListaJugadores) {
        this.ListaJugadores = ListaJugadores;
    }
    
    
}
