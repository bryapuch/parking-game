/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parking.game;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

/**
 *
 * @author Bryan
 */
public class vNivel3 {
    
    
    private Label marcador;
    private Label tiempo;
    private Label vida;
    private int puntos_ganados = 0;
    private double tiempo_transcurrido = 0;
    private Random random;
    private Pane gamePane;
    //astronauta
    private Auto auto;
    //piezas de la nave
    private ArrayList<Shape> piezas = new ArrayList<>();
    private ArrayList<Shape> estacionar = new ArrayList<>();
    //
    public RunnableTiempo hiloTiempo;
    public CrearPiezas cp;
    private Scene scene;
    Estacionar estacion;
    

    public vNivel3() {

    }

    public BorderPane crearPantalla(Stage stage) {
        StackPane v = new StackPane();
//        ImageView imageV;
//        imageV = new ImageView(new Image(vNivel1.class.getResource("suelos3.png").toExternalForm()));
//        imageV.setFitHeight(550);
//        imageV.setFitWidth(550);
        random = new Random();

        BorderPane root = new BorderPane();
        root.setTop(crearTop(stage));

        root.setCenter(crearCentro(stage));

        piezasJuego();

        cp = new CrearPiezas();
        (new Thread(cp)).start();
        tiempoterminado(stage);

        hiloTiempo = new RunnableTiempo();
        (new Thread(hiloTiempo)).start();

//        v.getChildren().add(imageV);
//        v.getChildren().add(root); 
        return root;

    }

    public AnchorPane crearTop(Stage stage) {
        //creamos un objeto de tipo FONT
        //esto se hace si se quiere cambiar el tipo y tamano de letra usado 
        //en un label
        Font theFont = Font.font("Helvetica", FontWeight.BOLD, 24);

        AnchorPane top = new AnchorPane();
        Label lt = new Label("Tiempo:");
        lt.setFont(theFont);
        Label lm = new Label("Marcador:");
        lm.setFont(theFont);
        tiempo = new Label("00:00");
        tiempo.setFont(theFont);
        marcador = new Label(String.valueOf(puntos_ganados));
        marcador.setFont(theFont);
        Label lbV = new Label("Vida: ");
        lbV.setFont(theFont);
        vida = new Label("3");
        vida.setFont(theFont);

        top.getChildren().addAll(lt, tiempo, lm, marcador, lbV, vida);

        AnchorPane.setLeftAnchor(lt, 10.0);
        AnchorPane.setLeftAnchor(tiempo, 110.0);
        AnchorPane.setRightAnchor(lm, 50.0);
        AnchorPane.setRightAnchor(marcador, 10.0);
        AnchorPane.setLeftAnchor(lbV, 160.0);
        AnchorPane.setLeftAnchor(vida, 230.0);
        tiempoterminado(stage);

        return top;
    }

    public Pane crearCentro(Stage stage) {
        //usamos como contenedor un Pane porque queremos colocar los elementos
        //de forma libre a los largo del Pane
        gamePane = new Pane();
        gamePane.setStyle("-fx-background-image: url('" + vNivel1.class.getResource("suelos3.png").toExternalForm() + "');"
                + "-fx-background-repeat: stretch;"
                + "-fx-background-size: " + Ventanas.GAME_WIDTH + " " + (Ventanas.GAME_HEIGHT) + "; "
                + "-fx-background-position: center center;");
        gamePane.setPrefSize(Ventanas.GAME_WIDTH, Ventanas.GAME_HEIGHT);

        //creamos el auto
        auto = new Auto();

        //anadimos el auto a la escena principal
        gamePane.getChildren().add(auto.getObjeto());
        //estacionar.add(estacionamiento());
        gamePane.getChildren().add(estacionamiento());

        //TODO: fijar al astronauta en una posicion aletoria dentro del espacio
        //generar x , y de forma aletorio
        double maximox = Ventanas.GAME_WIDTH - 70;
        double maximoy = Ventanas.GAME_HEIGHT - 90;
        double x = random.nextDouble() * maximox + 35;
        double y = random.nextDouble() * maximoy + 45;

        auto.fijarPosicionObjeto(x, y);
        tiempoterminado(stage);
        moverAuto(stage);
        System.out.println(auto.iauto.getLayoutY() - Ventanas.desplazamiento);
        System.out.println("x: " + estacion.getX() + " y:"
                + estacion.getY());

        //TODO: Crear varias pieza (10) y colocarlas en el panel en forma aletoria,
        //los shape representan la piezas del juego
        //por cada pieza recolectada gana 100 puntos
        return gamePane;

    }

    public void moverAuto(Stage stage) {
        vNivel2.sc2.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                switch (event.getCode()) {
                    case UP:
                        auto.getObjeto().setLayoutY(auto.getObjeto().getLayoutY() - Ventanas.desplazamiento);
                        chequearColisiones();
                        chequearEstacionamiento(stage);
                        tiempoterminado(stage);
                        System.out.println("x:" + auto.getObjeto().getLayoutX()
                                + "y: " + auto.getObjeto().getLayoutY());

                        break;
                    case RIGHT:
                        auto.getObjeto().setLayoutX(auto.getObjeto().getLayoutX() + Ventanas.desplazamiento);
                        chequearColisiones();
                        chequearEstacionamiento(stage);
                        tiempoterminado(stage);
                        System.out.println("x:" + auto.getObjeto().getLayoutX()
                                + "y: " + auto.getObjeto().getLayoutY());
                        break;
                    case DOWN:
                        auto.getObjeto().setLayoutY(auto.getObjeto().getLayoutY() + Ventanas.desplazamiento);
                        chequearColisiones();
                        chequearEstacionamiento(stage);
                        tiempoterminado(stage);
                        System.out.println("x:" + auto.getObjeto().getLayoutX()
                                + "y: " + auto.getObjeto().getLayoutY());
                        break;
                    case LEFT:
                        auto.getObjeto().setLayoutX(auto.getObjeto().getLayoutX() - Ventanas.desplazamiento);
                        chequearColisiones();
                        chequearEstacionamiento(stage);
                        tiempoterminado(stage);
                        System.out.println("x:" + auto.getObjeto().getLayoutX()
                                + "y: " + auto.getObjeto().getLayoutY());
                        break;
                }
            }
        });

    }

    public static boolean isCollision(Node n1, Node n2) {
        Bounds b1 = n1.getBoundsInParent();
        Bounds b2 = n2.getBoundsInParent();
        if (b1.intersects(b2)) {
            return true;
        } else {
            return false;
        }
    }

    private void chequearColisiones() {
        //chequeamos si hay una interseccion entre el astronauta y 
        //y algunas de las piezas, si lo hay debemos remover la pieza de la escena
        //y de la lista de piezas
        //como queremos iterar y remover a la vez, usamos un iterator
        Iterator<Shape> it = piezas.iterator();
        while (it.hasNext()) {
            //obtenemos el siguiente elemento de la piezas
            Shape p = it.next();
            if (isCollision(p, auto.getObjeto())) {
                gamePane.getChildren().remove(p);
                it.remove();
                //debemos aumentar los puntos del astronauta
                puntos_ganados += 100;
                marcador.setText(String.valueOf(puntos_ganados));
            }
        }
    }

    public void piezasJuego() {
        piezas.add(new Rectangle(50, 40, Color.RED));
        piezas.add(new Circle(40, Color.BLUE));
        piezas.add(new Circle(50, Color.YELLOW));
        piezas.add(new Rectangle(20, 40, Color.AQUA));
        piezas.add(new Circle(40, Color.BISQUE));
        piezas.add(new Rectangle(50, 40, Color.RED));
        piezas.add(new Circle(40, Color.BLUE));
        piezas.add(new Circle(50, Color.YELLOW));
        piezas.add(new Rectangle(20, 40, Color.AQUA));
        piezas.add(new Circle(40, Color.BISQUE));
        piezas.add(new Rectangle(35,20,Color.AQUAMARINE));
        piezas.add(new Circle(25,Color.YELLOWGREEN));
        piezas.add(new Rectangle(41,30,Color.BEIGE));
        piezas.add(new Circle(20,Color.CRIMSON));
    }

    //a este metodo se llama en el metod stop() de la aplicacion para para los hilo
    //cuando se cierre la aplicacion
    public void detenerHilos() {
        hiloTiempo.parar();
        cp.parar();
    }

    private Shape estacionamiento() {
        double x = random.nextDouble() * (Ventanas.GAME_WIDTH - 110) + 80;
        double y = random.nextDouble() * (Ventanas.GAME_HEIGHT - 110) + 80;
        Rectangle esta = new Rectangle(x, y, 120, 100);
        estacion = new Estacionar(x, y);
        esta.setFill(null);
        esta.setStroke(Color.RED);
        esta.setStrokeWidth(15);
        return esta;
    }

    private void chequearEstacionamiento(Stage stage) {
        Stage stage2 = new Stage();
        Scene scene;
        VBox vb = new VBox(20);
        HBox hb = new HBox(20);
        Label lbPuntaje = new Label();
        Label lbGanar = new Label();
        Label lbTiempo = new Label();
        Button btSalir = new Button("Salir");
        if ((auto.getObjeto().getLayoutX() < estacion.getX() + 30 && auto.getObjeto().getLayoutX() > estacion.getX() - 10)
                && (auto.getObjeto().getLayoutY() < estacion.getY() + 30 && auto.getObjeto().getLayoutY() > estacion.getY() - 10)) {
            lbGanar.setText("Has ganado el nivel 3");
            lbPuntaje.setText("Puntaje: " + marcador.getText());
            lbTiempo.setText("Tiempo: " + tiempo.getText());
            vb.getChildren().addAll(lbGanar, lbPuntaje, lbTiempo);
            hb.getChildren().add(btSalir);
            vb.getChildren().add(hb);
            vb.setAlignment(Pos.CENTER);
            hb.setAlignment(Pos.CENTER);
            scene = new Scene(vb, 300, 300);
            stage2.setTitle("Parking Game - Level 3 ");
            stage2.setScene(scene);
            stage2.show();
            hiloTiempo.parar();

            btSalir.setOnAction((EventHandler) -> {
                stage2.close();
                String nombre = Ventanas.jugador.getNombre();
                String fecha = Ventanas.jugador.getFecha();
                String tie = tiempo.getText().toString();
                LecturayEscritura.escribirDatosJugador(new Jugador(nombre, "00:" + tie, marcador.getText(), fecha));
                Ventanas v = new Ventanas();
                Scene cene = new Scene(v.MenuPrincipal(stage));
                stage.setTitle("Parking Game!");
                stage.setScene(cene);
                stage.setResizable(false);
                stage.show();

            });
        }
    }

    public void vidas() {

    }

    public void tiempoterminado(Stage stage) {
        Stage pri = new Stage();
        VBox vb = new VBox(20);
        Label lbmen = new Label();
        Button btMenu = new Button("Menu Principal");
        if (tiempo.getText().toString().equals("60.0")) {
            lbmen.setText("El tiempo se ha terminado"
                    + "\n Intentelo de nuevo");
            lbmen.setFont(Font.font(20));
            btMenu.setFont(Font.font(20));
            vb.getChildren().addAll(lbmen, btMenu);
            vb.setAlignment(Pos.CENTER);
            Scene sc = new Scene(vb, 300, 300);
            pri.setTitle("Fin del Juego");
            pri.setScene(sc);
            pri.show();

            btMenu.setOnAction(EventHanlder -> {
                pri.close();
                String nombre = Ventanas.jugador.getNombre();
                String fecha = Ventanas.jugador.getFecha();
                String tie = tiempo.getText().toString();
                LecturayEscritura.escribirDatosJugador(new Jugador(nombre, "00:" + tie, marcador.getText(), fecha));
                Ventanas v = new Ventanas();
                Scene cene = new Scene(v.MenuPrincipal(stage));
                stage.setTitle("Parking Game!");
                stage.setScene(cene);
                stage.setResizable(false);
                stage.show();
            });

        }
    }

    private class CrearPiezas implements Runnable {

        private boolean detener = false;

        @Override
        public void run() {
            //crear la piezas cada cierto tiempo
            //y ubicarlas en el panel
            //el hilo se termina cuand no hay mas piezas que poner o se 
            //ha llamado a detener
            int i = 0;
            while (!detener && i < piezas.size()) {

                Shape p = piezas.get(i);
                double x = random.nextDouble() * (Ventanas.GAME_WIDTH - 100) + 40;
                double y = random.nextDouble() * (Ventanas.GAME_HEIGHT - 100) + 40;

                Platform.runLater(() -> {
                    gamePane.getChildren().add(p);
                    p.setLayoutX(x);
                    p.setLayoutY(y);
                });

                try {
                    Thread.sleep((long) (random.nextDouble() * 10000.0));
                } catch (InterruptedException ex) {
                    Logger.getLogger(vNivel1.class.getName()).log(Level.SEVERE, null, ex);
                }

                i++;
            }

        }

        public void parar() {
            detener = true;
        }

    }

    //MANEJAR EL TIEMPO TRANSCURRIDO
    private class RunnableTiempo implements Runnable {

        private boolean detener = false;

        //TODO: MODIFICAR EL PROGRAMA PARA QUE EN CASO QUE NO QUEDEN MAS PIEZAS EN EL
        //POR RECOLECTAR SE ACABE EL HILO
        public void run() {
            while (!detener) {
                tiempo_transcurrido += 0.5;
                if (tiempo_transcurrido >= 60) {
                    detener = true;
                }

                Platform.runLater(() -> {
                    tiempo.setText(String.valueOf(tiempo_transcurrido));
                });

                try {
                    Thread.sleep(500);
                } catch (InterruptedException ex) {
                    Logger.getLogger(vNivel1.class.getName()).log(Level.SEVERE, null, ex);
                }

            }

            //cuando se termina el tiempo se debe mostrar la nueva ventana con los
            //puntos alcanzados
            if (tiempo_transcurrido >= 120) {
                //cambiamo el contendor raiz da la escena con el contendor
                //raiz dle panel de los resultado
                PanelResultados pr = new PanelResultados(puntos_ganados, tiempo_transcurrido);
                ParkingGame.scene.setRoot(pr.getRoot());
            }

        }

        public void parar() {
            detener = true;
        }
    }
    
}
