/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parking.game;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Bryan
 */
public class LecturayEscritura {

    public static String archivoJugador = "src/parking/game/jugadores.txt";

    public static void escribirDatosJugador(Jugador e) {
        try (BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(archivoJugador, true), "utf-8"))) {

            out.write(e.getNombre() + "," + e.getTiempo() + "," + e.getPuntaje() + "," + e.getFecha() + "\n");

        } catch (FileNotFoundException ex) {
            Logger.getLogger(LecturayEscritura.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(LecturayEscritura.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ObservableList<Jugador> getJugador() {
        ObservableList<Jugador> ListaJugadores = FXCollections.observableArrayList();
        try (FileReader reader = new FileReader(new File(archivoJugador));
                BufferedReader in = new BufferedReader(reader);) {
            String line;
            while ((line = in.readLine()) != null) {
                String[] jugador = line.split(",");
                String nombre = jugador[0];
                String tiempo = jugador[1];
                String puntaje = jugador[2];
                String fecha = jugador[3];
                Jugador jug = new Jugador(nombre, tiempo, puntaje, fecha);
                ListaJugadores.add(jug);
            }
        } catch (IOException ex) {
            Logger.getLogger(LecturayEscritura.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ListaJugadores;
    }

}

